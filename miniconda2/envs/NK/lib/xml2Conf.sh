#
# Configuration file for using the XML library in GNOME applications
#
XML2_LIBDIR="-L/ampa/stephanie/miniconda2/envs/NK/lib"
XML2_LIBS="-lxml2 -lz   -lm "
XML2_INCLUDEDIR="-I/ampa/stephanie/miniconda2/envs/NK/include/libxml2"
MODULE_VERSION="xml2-2.9.2"

