#
# Configuration file for using the xslt library
#
XSLT_LIBDIR="-L/ampa/stephanie/miniconda2/envs/NK/lib"
XSLT_LIBS="-lxslt  -L/ampa/stephanie/miniconda2/envs/NK/lib -lxml2 -lz -lm -ldl -lm -lrt"
XSLT_INCLUDEDIR="-I/ampa/stephanie/miniconda2/envs/NK/include"
MODULE_VERSION="xslt-1.1.28"
