https://repo.continuum.io/pkgs/free/linux-64/_cache-0.0-py27_x0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/python-2.7.11-0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/conda-env-2.4.5-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/openssl-1.0.2d-0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/pycosat-0.6.1-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/pyyaml-3.11-py27_1.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/readline-6.2-2.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/requests-2.9.0-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/sqlite-3.8.4.1-1.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/tk-8.5.18-0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/yaml-0.1.6-0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/zlib-1.2.8-0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/conda-3.19.0-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/pycrypto-2.6.1-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/pip-7.1.2-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/wheel-0.26.0-py27_1.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/setuptools-18.8.1-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/libgfortran-1.0-0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/libpng-1.6.17-0.tar.bz2
https://conda.binstar.org/neurokernel/channel/ubuntu1404/linux-64/openmpi-1.10.1-cuda_1.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/openssl-1.0.2f-0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/pixman-0.32.6-0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/sqlite-3.9.2-0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/freetype-2.5.5-0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/hdf5-1.8.15.1-2.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/libxml2-2.9.2-0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/openblas-0.2.14-3.tar.bz2
https://conda.binstar.org/neurokernel/channel/ubuntu1404/linux-64/bidict-0.10.0.post1-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/cython-0.23.4-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/decorator-4.0.6-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/dill-0.2.4-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/fontconfig-2.11.1-5.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/funcsigs-0.4-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/futures-3.0.3-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/libxslt-1.1.28-0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/markupsafe-0.23-py27_0.tar.bz2
https://conda.binstar.org/neurokernel/channel/ubuntu1404/linux-64/mpi4py-2.0.0-py27_openmpi1101_1.tar.bz2
https://conda.binstar.org/neurokernel/channel/ubuntu1404/linux-64/msgpack-python-0.4.6-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/nose-1.3.7-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/numpy-1.10.2-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/ply-3.8-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/psutil-3.4.2-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/py-1.4.31-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/pyparsing-2.0.3-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/pytz-2015.7-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/setuptools-19.4-py27_0.tar.bz2
https://conda.binstar.org/neurokernel/channel/ubuntu1404/linux-64/shutilwhich-1.1.0-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/sip-4.16.9-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/six-1.10.0-py27_0.tar.bz2
https://conda.binstar.org/neurokernel/channel/ubuntu1404/linux-64/twiggy-0.4.7-py27_0.tar.bz2
https://conda.binstar.org/neurokernel/channel/ubuntu1404/linux-64/appdirs-1.4.0-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/cairo-1.12.18-6.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/cycler-0.9.0-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/h5py-2.5.0-np110py27_4.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/lxml-3.5.0-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/mako-1.0.3-py27_0.tar.bz2
https://conda.binstar.org/neurokernel/channel/ubuntu1404/linux-64/msgpack-numpy-0.3.6-np110py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/networkx-1.10-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/numexpr-2.4.6-np110py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/pip-8.0.1-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/pytest-2.8.5-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/python-dateutil-2.4.2-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/qt-4.8.7-1.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/scipy-0.16.1-np110py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/pandas-0.17.1-np110py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/pbr-1.3.0-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/pycairo-1.10.0-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/pyqt-4.11.4-py27_1.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/pytables-3.2.2-np110py27_0.tar.bz2
https://conda.binstar.org/neurokernel/channel/ubuntu1404/linux-64/pytools-2015.1.6-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/matplotlib-1.5.1-np110py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/mock-1.3.0-py27_0.tar.bz2
https://conda.binstar.org/neurokernel/channel/ubuntu1404/linux-64/pycuda-2015.1.3-np110py27_cuda75_0.tar.bz2
https://conda.binstar.org/neurokernel/channel/ubuntu1404/linux-64/scikit-cuda-0.5.1-py27_0.tar.bz2
https://conda.binstar.org/neurokernel/channel/ubuntu1404/linux-64/neurokernel_deps-0.1.0-8.tar.bz2
https://conda.binstar.org/neurokernel/channel/ubuntu1404/linux-64/pycuda-2015.1.3-np110py27_cuda70_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/ipython_genutils-0.1.0-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/path.py-8.1.2-py27_1.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/pexpect-3.3-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/setuptools-19.6.2-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/simplegeneric-0.8.1-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/pickleshare-0.5-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/pip-8.0.2-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/traitlets-4.1.0-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/ipython-4.0.3-py27_0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/mkl-11.3.1-0.tar.bz2
https://repo.continuum.io/pkgs/free/linux-64/numpy-1.10.4-py27_0.tar.bz2
