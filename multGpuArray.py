
import pycuda.autoinit
from pycuda.compiler import SourceModule
import pycuda.gpuarray as gpuarray
import pycuda.driver as drv
import numpy as np

#### The Kernel ####
mod = SourceModule("""
__global__ void multiply_them(int *c, int *a, int *b, int m, int n, int p)
{
  int c_value = 0;
  int row = blockIdx.y *  blockDim.y + threadIdx.y; 
  int col = blockIdx.x *  blockDim.x + threadIdx.x;
  for (int i = 0; i < n; i++)
    c_value += a[row * n + i] * b[i * p + col];
  c[row * p + col] = c_value;
}
""", options = ["--ptxas-options=-v"])

a = (10*np.random.randn(20)).astype(np.int32).reshape(4,5)
b = (10*np.random.randn(30)).astype(np.int32).reshape(5,6)
c = np.dot(a, b)

a_gpu = gpuarray.to_gpu(a)
b_gpu = gpuarray.to_gpu(b)
c_gpu = gpuarray.empty((4,6), np.int32)
'''note: multiplying to create a 4x6 array, will need 24 threads, so only 1 block'''

m = 4
n = 5
p = 6

multiply_func = mod.get_function("multiply_them")
multiply_func.prepare('PPPiii')
multiply_func.prepared_call((1,1), (6,4,1), c_gpu.gpudata, a_gpu.gpudata, b_gpu.gpudata, m, n, p)
#        c_gpu, a_gpu, b_gpu, m,  n, p,
#        block=(4,6,1), grid=(1, 1)) 

print "\nArray A:\n" , a_gpu
print "\nArray B:\n", b_gpu
print "________________________________________________\n"
print "Correct Matrix Multiplication Product:\n", c
print "\nParallel Matrix Multiplication Product:\n", c_gpu
