import pycuda.autoinit
from pycuda.compiler import SourceModule
import pycuda.gpuarray as gpuarray
import pycuda.driver as drv
import numpy as np
from pycuda.tools import dtype_to_ctype 

n = 16
shape = (n,n)
array_type = np.float32

a = np.random.randn(n*n).astype(array_type).reshape(shape)
b = np.random.randn(n*n).astype(array_type).reshape(shape)
c = np.dot(a, b)

a_gpu = gpuarray.to_gpu(a)
b_gpu = gpuarray.to_gpu(b)
c_gpu = gpuarray.empty(shape, array_type)

#### The Kernel ####
mod = SourceModule("""
__global__ void multiply_them(%(type)s *c, %(type)s *a, %(type)s *b)
{

  __shared__ %(type)s As[%(N)s][%(N)s];
  __shared__ %(type)s Bs[%(N)s][%(N)s];

  float c_value = 0;
  int row = blockIdx.y *  blockDim.y + threadIdx.y;
  int col = blockIdx.x *  blockDim.x + threadIdx.x;

  As[threadIdx.y][threadIdx.x] = a[row * %(N)s + threadIdx.x];
  Bs[threadIdx.y][threadIdx.x] = b[threadIdx.y * %(N)s + col];
  __syncthreads();

  for (int i = 0; i < %(N)s; i++)
  {
    c_value += As[threadIdx.y][i] * Bs[i][threadIdx.x];
  }
  c[row * %(N)s + col] = c_value;
}
""" % {"type": dtype_to_ctype(array_type), "N": n})

multiply_func = mod.get_function("multiply_them")
multiply_func.prepare('PPP')
multiply_func.prepared_call((1,1,1), (n,n), c_gpu.gpudata, a_gpu.gpudata, b_gpu.gpudata)

print "\nArray A:\n" , a_gpu
print "\nArray B:\n", b_gpu
print "________________________________________________\n"
print "Correct Matrix Multiplication Product:\n", c
print "\nParallel Matrix Multiplication Product:\n", c_gpu
