import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule

#### The Kernal ####
mod = SourceModule("""
__global__ void multiply_them(float *dest, float *a, float *b)
{
  const int i = threadIdx.x;
  dest[i] = a[i] * b[i];
}
""")

multiply_them = mod.get_function("multiply_them")

a = np.random.randn(400).astype(np.float32)  # 1D array of 400 random values
b = np.random.randn(400).astype(np.float32)  # 1D array of 400 random values

dest = np.zeros_like(a)  # creates array of same shape as a but only containing 0s
multiply_them(
        drv.Out(dest), drv.In(a), drv.In(b),  # dest will be copied from device after invoking kernal, a & b will be copied to device  (shortcut for Explicit Memory Copies)
        block=(400, 1, 1), grid=(1, 1))  #2D grid, with 3D block with 400 threads per block

print dest-a*b  # should print array of 0s
