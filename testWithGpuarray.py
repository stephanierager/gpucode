import pycuda.autoinit
import pycuda.gpuarray as gpuarray
import numpy as np

a = np.random.randn(400).astype(np.float32)  # 1D array of 400 random values
a_gpu = gpuarray.to_gpu(a)

b = np.random.randn(400).astype(np.float32)  # 1D array of 400 random values
b_gpu = gpuarray.to_gpu(b)

c = (a_gpu*b_gpu).get()
print c - a*b
