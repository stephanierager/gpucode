import pycuda.autoinit
from pycuda.compiler import SourceModule
import pycuda.gpuarray as gpuarray
import pycuda.driver as drv
import numpy as np
from pycuda.tools import dtype_to_ctype

n = 5
m = n
array_type = np.int64

a = (np.random.randn(n*m)*100).astype(array_type).reshape((m,n))
a_gpu = gpuarray.to_gpu(a)
c_gpu = gpuarray.empty((n,m), array_type)

#### The Kernel ####
mod = SourceModule("""
__global__ void transpose(%(type)s *in, %(type)s *out)
{

  __shared__ %(type)s temp[%(N)s][%(M)s];
  
  int tIDx = threadIdx.x;
  int tIDy = threadIdx.y;
  int bIDx = blockIdx.x;
  int bIDy = blockIdx.x;
  int width = gridDim.x * blockDim.x;
  
  int col = bIDx *  blockDim.x + tIDx;
  int row = bIDy *  blockDim.y + tIDy;

  if(tIDx < %(N)s && tIDy < %(N)s)
  { 
    temp[tIDy][tIDx] = in[row* width + col];  
  }
  __syncthreads();
  
  col = bIDy * blockDim.y + tIDx;
  row = bIDx * blockDim.x + tIDy;
  
  if(tIDx < %(N)s && tIDy < %(N)s)
  {
    out[row* width + col] = temp[tIDx][tIDy];
  }
  __syncthreads();
}
""" % {"type": dtype_to_ctype(array_type), "N": n, "M": m})

threadsPerBlock = 5
blocksPerGrid = (n + threadsPerBlock - 1) / threadsPerBlock

transpose_func = mod.get_function("transpose")
transpose_func.prepare('PP')
transpose_func.prepared_call((blocksPerGrid,blocksPerGrid,1), (threadsPerBlock,threadsPerBlock,1), a_gpu.gpudata, c_gpu.gpudata)

print "\nOriginal Array:\n" , a_gpu
print "\nTransposed Array:\n", c_gpu
